import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import minimize
from scipy.ndimage import binary_erosion

import tensorflow as tf

def getCoeffs(curve,t,numcoeffs):
    re = curve[:,0]
    im = curve[:,1]
    irange = int((numcoeffs-1)/2)

    i = np.array([int(x) for x in np.linspace(-irange, irange, num=numcoeffs) if x != 0])

    cr = []
    ci = []

    for k in i:
        sin_term = np.sin(-k*2*math.pi*t)
        cos_term = np.cos(-k*2*math.pi*t)
        cr.append((re@cos_term-im@sin_term)/t.shape[0])
        ci.append((re@sin_term+im@cos_term)/t.shape[0])

    return np.array(cr), np.array(ci)


def getCoeffs_vectorized_nonzero(curve_tensor, t, numcoeffs):
    #print(numcoeffs)
    #print(t.shape)
    h, w, l = curve_tensor.shape
    irange = int((numcoeffs-1)/2)

    re = curve_tensor[:,:,:l:2]
    im = curve_tensor[:,:,1:l:2]

    t_tensor = np.zeros((t.shape[0],numcoeffs-1))
    t_tensor[:] = t
    

    i = np.array([int(x) for x in np.linspace(-irange, irange, num=numcoeffs) if x != 0])

    res = np.zeros((h,w,numcoeffs*2-2))

    
    
    for k in range(0,len(i)):
        t_tensor = np.zeros((t.shape[0],1))
        t_tensor[:] = t
        t_tensor *= -i[k]*2*math.pi
        res[:,:,2*k] = ((re@np.cos(t_tensor)-im@np.sin(t_tensor))/(t_tensor.shape[0]))[:,:,0]
        res[:,:,2*k+1] = ((re@np.sin(t_tensor)+im@np.cos(t_tensor))/(t_tensor.shape[0]))[:,:,0]
    
    return res


def fourier_curve(t,coeffs):
    coeff_x, coeff_y = coeffs
    coeff_x = coeff_x.reshape((-1,))
    coeff_y = coeff_x.reshape((-1,))
    num_coeffs = coeff_x.shape[0]
    imin = int(-1*(num_coeffs-1)/2)
    imax = -1*imin
    scalars = np.linspace(imin,imax,num=2*imax+1)
    scalars = np.reshape(scalars,(1,num_coeffs))
    scalars *= 2*math.pi

    args = t@scalars
    x_orig = np.cos(args)
    y_orig = np.sin(args)
    x = coeff_x*x_orig-coeff_y*y_orig
    y = coeff_x*y_orig+coeff_y*x_orig

    x = np.sum(x,axis=1)
    y = np.sum(y,axis=1)
    return np.array([x,y])

def fourier_curve_nonzero(t,coeffs):
    coeff_x, coeff_y = coeffs
    coeff_x = np.insert(coeff_x,int(coeff_x.shape[0]/2),0,axis=0)
    coeff_y = np.insert(coeff_y,int(coeff_y.shape[0]/2),0,axis=0)
    #print(coeff_x.shape)
    num_coeffs = coeff_x.shape[0]
    imin = int(-1*(num_coeffs-1)/2)
    imax = -1*imin
    scalars = np.linspace(imin,imax,num=2*imax+1)
    scalars = np.reshape(scalars,(1,num_coeffs))
    scalars *= 2*math.pi

    args = t@scalars
    x_orig = np.cos(args)
    y_orig = np.sin(args)
    x = coeff_x*x_orig-coeff_y*y_orig
    y = coeff_x*y_orig+coeff_y*x_orig

    x = np.sum(x,axis=1)
    y = np.sum(y,axis=1)
    return np.array([x,y])

def fourier_curve_vectorized(t, coeff_tensor):
    _,_,_,c = coeff_tensor.shape
    #print([n,h,w,c])
    coeff_x = coeff_tensor[...,:c:2]
    coeff_y = coeff_tensor[...,1:c:2]
    
    num_coeffs = int(c/2)
    imin = int(-1*(num_coeffs-1)/2)
    scalars = tf.linspace(imin,-1*imin,num=-2*imin+1)
    scalars = tf.reshape(scalars,(1,num_coeffs))
    scalars *= 2*math.pi
    args = tf.transpose(tf.tensordot(t,scalars,axes=1))
    
    #args = np.load('./args.npy')
    #args = tf.convert_to_tensor(args, dtype=tf.float32)
    args = tf.math.multiply(tf.ones([tf.shape(coeff_tensor)[0],tf.shape(coeff_tensor)[1],tf.shape(coeff_tensor)[2],1,1]),tf.cast(args,tf.float32))
    #args = tf.reshape(tf.tile(args,tf.constant([n,h,w,1,1],dtype=tf.int32)))

    coeff_x = tf.reshape(coeff_x,(tf.shape(coeff_tensor)[0],tf.shape(coeff_tensor)[1],tf.shape(coeff_tensor)[2],int(c/2),1))
    coeff_y = tf.reshape(coeff_y,(tf.shape(coeff_tensor)[0],tf.shape(coeff_tensor)[1],tf.shape(coeff_tensor)[2],int(c/2),1))

    x_orig = tf.math.cos(args)
    y_orig = tf.math.sin(args)
    x = coeff_x*x_orig-coeff_y*y_orig
    y = coeff_x*y_orig+coeff_y*x_orig

    x = tf.math.reduce_sum(x,axis=3)
    y = tf.math.reduce_sum(y,axis=3)
    return tf.stack((x,y),axis=4)

def fourier_curve_vectorized_nonzero(t, coeff_tensor):
    _,_,_,c = coeff_tensor.shape
    num_coeffs = int(c/2)
    z = tf.zeros([tf.shape(coeff_tensor)[0],tf.shape(coeff_tensor)[1],tf.shape(coeff_tensor)[2],2], tf.float32)


    cnew = tf.concat([tf.concat([coeff_tensor[:,:,:,:num_coeffs],z],axis=3),coeff_tensor[:,:,:,num_coeffs:]],axis=3)
    coeff_tensor = cnew


    c = c+2
    num_coeffs = int(c/2)

    coeff_x = coeff_tensor[...,:c:2]
    coeff_y = coeff_tensor[...,1:c:2]
    
    
    imin = int(-1*(num_coeffs-1)/2)
    scalars = tf.linspace(imin,-1*imin,num=-2*imin+1)
    scalars = tf.reshape(scalars,(1,num_coeffs))
    scalars *= 2*math.pi
    args = tf.transpose(tf.tensordot(t,scalars,axes=1))
    

    args = tf.math.multiply(tf.ones([tf.shape(coeff_tensor)[0],tf.shape(coeff_tensor)[1],tf.shape(coeff_tensor)[2],1,1]),tf.cast(args,tf.float32))

    coeff_x = tf.reshape(coeff_x,(tf.shape(coeff_tensor)[0],tf.shape(coeff_tensor)[1],tf.shape(coeff_tensor)[2],int(c/2),1))
    coeff_y = tf.reshape(coeff_y,(tf.shape(coeff_tensor)[0],tf.shape(coeff_tensor)[1],tf.shape(coeff_tensor)[2],int(c/2),1))

    x_orig = tf.math.cos(args)
    y_orig = tf.math.sin(args)
    x = coeff_x*x_orig-coeff_y*y_orig
    y = coeff_x*y_orig+coeff_y*x_orig

    x = tf.math.reduce_sum(x,axis=3)
    y = tf.math.reduce_sum(y,axis=3)
    return tf.stack((x,y),axis=4)
