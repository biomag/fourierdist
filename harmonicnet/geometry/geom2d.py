from __future__ import print_function, unicode_literals, absolute_import, division
import numpy as np
import warnings

from skimage.measure import regionprops
from skimage.draw import polygon
from csbdeep.utils import _raise

from ..utils import path_absolute, _is_power_of_2, _normalize_grid
from ..matching import _check_label_array

from ..fourier.fourier_toolkit import getCoeffs, fourier_curve_vectorized_nonzero

from scipy.interpolate import interp1d
import cv2

import skimage.morphology

def coeff_tensor(a, n_coeffs=5, contoursize=1000):
    """'a' assumbed to be a label image with integer values that encode object ids. id 0 denotes background."""

    (n_coeffs >= 3 and n_coeffs%2==1) or _raise(ValueError("need 'n_coeffs' >= 3 and odd numbers"))

    dist = np.zeros((a.shape[0], a.shape[1], n_coeffs, 2))
    # dist = np.zeros((a.shape[0],a.shape[1],int(n_coeffs/2),2))

    obj_list = np.unique(a)
    obj_list = obj_list[1:]

    for i in range(len(obj_list)):
        mask_temp = a.copy()
        mask_temp[mask_temp != obj_list[i]] = 0
        mask_temp[mask_temp > 0] = 1

        contour = contour_cv2_mask_uniform(mask_temp, contoursize)
        cm = np.mean(contour, axis=0)
        contour -= contour[0,:]
	    #contour -= cm
        # print(cm)

        t = np.reshape(np.linspace(0, 1, num=contoursize), (-1, 1))
        cx, cy = getCoeffs(contour, t, n_coeffs)

        rel_coord = np.empty((a.shape[0], a.shape[1], 2), dtype=dist.dtype)
        start = np.indices((a.shape[0], a.shape[1]))
        rel_coord[...,0] = cm[0]-start[0,...]
        rel_coord[...,1] = cm[1]-start[1,...]


        idx_nonzero = np.argwhere(mask_temp)

        dist[idx_nonzero[:, 0], idx_nonzero[:, 1], :n_coeffs-1, 0] = cx[:, 0]
        dist[idx_nonzero[:, 0], idx_nonzero[:, 1], :n_coeffs-1, 1] = cy[:, 0]
        dist[idx_nonzero[:, 0], idx_nonzero[:, 1], n_coeffs - 1, 0] = rel_coord[idx_nonzero[:, 0], idx_nonzero[:, 1], 0]
        dist[idx_nonzero[:, 0], idx_nonzero[:, 1], n_coeffs - 1, 1] = rel_coord[idx_nonzero[:, 0], idx_nonzero[:, 1], 1]


    dist = np.reshape(dist, (dist.shape[0], dist.shape[1], -1))
    return dist


def polygons_to_label(coord, prob, points, shape=None, for_pred=False, thr=-np.inf):
    sh = coord.shape[:2] if shape is None else shape
    lbl = np.zeros(sh, np.int32)
    #tmp = np.zeros(sh, np.int32)
    # sort points with increasing probability
    ind = np.argsort([prob[p[0], p[1]] for p in points])
    points = points[ind]

    i = 1
    for p in points:
        if prob[p[0], p[1]] < thr:
            continue
        rr, cc = polygon(coord[p[0], p[1], 0], coord[p[0], p[1], 1], sh)
        # lbl[rr, cc] = i

        if not for_pred:
            lbl[rr, cc] = i
        else:
            tmp = np.zeros(sh, np.int32)
            tmp[rr,cc] = 255
            tmp = skimage.morphology.binary_dilation(tmp, skimage.morphology.disk(1))
            lbl[tmp!=0] = i

        i += 1

    return lbl


def relabel_image_fourier(lbl, n_coeffs, contoursize, **kwargs):
    """relabel each label region in `lbl` with its Fourier representation"""
    _check_label_array(lbl, "lbl")
    if not lbl.ndim == 2:
        raise ValueError("lbl image should be 2 dimensional")
    # sd = time.time()
    dist = coeff_tensor(lbl, n_coeffs, contoursize, **kwargs)

    t = np.linspace(0, 1, num=contoursize)
    t = np.reshape(t, (-1, 1))

    coord = dist_to_coord(dist, contoursize)


    points = np.array(tuple(np.array(r.centroid).astype(int) for r in regionprops(lbl)))
    return polygons_to_label(coord, np.ones_like(lbl), points, shape=lbl.shape)


def dist_to_coord(dist, contoursize=50, grid=(1, 1)):
    """convert from Fourier descriptors to cartesian coordinates for a single image (3-D array) or multiple images (4-D array)"""
    grid = _normalize_grid(grid, 2)
    is_single_image = dist.ndim == 3
    if is_single_image:
        dist = np.expand_dims(dist, 0)
    assert dist.ndim == 4

    n_images, h, w, n_coeffs = dist.shape

    coord = np.empty((n_images, h, w, 2, contoursize), dtype=dist.dtype)

    start = np.indices((h, w))
    for i in range(2):
        coord[..., i, :] = grid[i] * np.broadcast_to(start[i].reshape(1, h, w, 1), (n_images, h, w, contoursize))

    t = np.linspace(0, 1, num=contoursize)
    t = np.reshape(t, (contoursize, 1))

    rel_trans = dist[...,n_coeffs-2:]

    newtrans = np.empty(coord.shape)
    newtrans[..., 0, :] = np.reshape(rel_trans[..., 0], (n_images,h,w,1))
    newtrans[..., 1, :] = np.reshape(rel_trans[..., 1], (n_images,h,w,1))

    coord += np.swapaxes(fourier_curve_vectorized_nonzero(t, dist[..., :n_coeffs-2]), 3, 4) + newtrans


    return coord[0] if is_single_image else coord


def contour_cv2_mask_uniform(mask, contoursize_max):
    mask = mask.astype(np.uint8)
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(cnt) for cnt in contours]
    max_ind = np.argmax(areas)
    contour = np.squeeze(contours[max_ind])
    contour = np.reshape(contour, (-1, 2))
    contour = np.append(contour, contour[0].reshape((-1, 2)), axis=0)
    contour = contour.astype('float32')

    rows, cols = mask.shape
    delta = np.diff(contour, axis=0)
    s = [0]
    for d in delta:
        dl = s[-1] + np.linalg.norm(d)
        s.append(dl)

    if (s[-1] == 0):
        s[-1] = 1

    s = np.array(s) / s[-1]
    fx = interp1d(s, contour[:, 0] / rows, kind='linear')
    fy = interp1d(s, contour[:, 1] / cols, kind='linear')
    S = np.linspace(0, 1, contoursize_max, endpoint=False)
    X = rows * fx(S)
    Y = cols * fy(S)

    contour = np.transpose(np.stack([X, Y])).astype(np.float32)

    contour = np.stack((contour[:, 1], contour[:, 0]), axis=-1)
    return contour
