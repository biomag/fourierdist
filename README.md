# HarmonicNet

This repository contains the source code for the article HarmonicNet: Fully automatic cell segmentation with Fourier descriptors. You can start using the library and run the example notebooks by installing the requirements from the *requirements.txt*:

	pip install -r requirements.txt



